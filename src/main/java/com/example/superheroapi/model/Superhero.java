package com.example.superheroapi.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "superheroes")
public class Superhero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "phone")
    private String phone;

    @Column(name = "image_path")
    private String imagePath;

    @ManyToMany
    @JoinTable(
        name = "superhero_affectations",
        joinColumns = @JoinColumn(name = "superhero_id"),
        inverseJoinColumns = @JoinColumn(name = "incidentType_id"))
    private Set<IncidentType> incidentAffectations = new HashSet<>();

    @OneToMany(mappedBy = "affectedSuperhero")
    private Set<DeclaredIncident> declaredIncidents = new HashSet<>();

    @Column(name = "isAvailable")
    private boolean isAvailable = true;

    public Superhero(){
        super();
    }

    public Superhero(String name, double latitude, double longitude, String phone, String imagePath, Set<IncidentType> incidentAffectations, Set<DeclaredIncident> declaredIncidents, boolean isAvailable) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.imagePath = imagePath;
        this.incidentAffectations = incidentAffectations;
        this.declaredIncidents = declaredIncidents;
        this.isAvailable = isAvailable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Set<IncidentType> getIncidentAffectations() {
        return incidentAffectations;
    }

    public void setIncidentAffectations(Set<IncidentType> incidentAffectations) {
        this.incidentAffectations = incidentAffectations;
    }

    public Set<DeclaredIncident> getDeclaredIncidents() {
        return declaredIncidents;
    }

    public void setDeclaredIncidents(Set<DeclaredIncident> declaredIncidents) {
        this.declaredIncidents = declaredIncidents;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public double[] getGeometry(){
        double[] geometry = new double[2];
        geometry[0] = getLongitude();
        geometry[1] = getLatitude();

        return geometry;
    }

    public String[] getProperties(){
        String[] properties = new String[2];
        properties[0] = getName();
        properties[1] = getPhone();

        return properties;
    }
}
