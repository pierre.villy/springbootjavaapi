package com.example.superheroapi.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "incident_types")
public class IncidentType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "incidentAffectations")
    private Set<Superhero> affectedSuperheroes;

    public IncidentType() {
    }

    public IncidentType(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
