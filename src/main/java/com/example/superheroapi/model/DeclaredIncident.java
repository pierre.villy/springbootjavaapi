package com.example.superheroapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "declared_incidents")
public class DeclaredIncident {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "description")
    private String description;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "resolution_date")
    private Date resolutionDate = null;

    @Column(name = "city")
    private String city;

    @ManyToOne
    @JoinColumn(name = "incidentType_id")
    @JsonBackReference
    private IncidentType incidentType;

    @ManyToOne
    @JoinColumn(name = "superhero_id")
    @JsonBackReference
    private Superhero affectedSuperhero;

    @Column(name = "isResolved")
    private boolean isResolved = false;

    public DeclaredIncident(){
    }

    public DeclaredIncident(double latitude, double longitude, String description, Date creationDate, Date resolutionDate, String city, IncidentType incidentType, Superhero affectedSuperhero, boolean isResolved) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.creationDate = creationDate;
        this.resolutionDate = resolutionDate;
        this.city = city;
        this.incidentType = incidentType;
        this.affectedSuperhero = affectedSuperhero;
        this.isResolved = isResolved;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getResolutionDate() {
        return resolutionDate;
    }

    public void setResolutionDate(Date resolutionDate) {
        this.resolutionDate = resolutionDate;
    }

    public String getCity(){return city;}

    public void setCity(String city){this.city = city;}

    public IncidentType getIncidentType() {
        return incidentType;
    }

    public void setIncidentType(IncidentType incidentType) {
        this.incidentType = incidentType;
    }

    public Superhero getAffectedSuperhero() {
        return affectedSuperhero;
    }

    public void setAffectedSuperhero(Superhero affectedSuperhero) {
        this.affectedSuperhero = affectedSuperhero;
    }

    public boolean isResolved() {
        return isResolved;
    }

    public void setResolved(boolean resolved) {
        isResolved = resolved;
    }
}
