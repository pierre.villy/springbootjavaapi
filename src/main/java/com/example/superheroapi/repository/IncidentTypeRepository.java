package com.example.superheroapi.repository;

import com.example.superheroapi.model.IncidentType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IncidentTypeRepository extends JpaRepository<IncidentType, Integer> {
}
