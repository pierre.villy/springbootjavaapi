package com.example.superheroapi.repository;

import com.example.superheroapi.model.DeclaredIncident;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeclaredIncidentRepository extends JpaRepository<DeclaredIncident, Integer> {
}
