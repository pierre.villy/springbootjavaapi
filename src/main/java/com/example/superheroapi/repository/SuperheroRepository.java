package com.example.superheroapi.repository;

import com.example.superheroapi.DTO.SuperheroDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.superheroapi.model.Superhero;


@Repository
public interface SuperheroRepository extends JpaRepository<Superhero, Integer> {
}
