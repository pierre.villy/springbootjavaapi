package com.example.superheroapi.DTO;

import com.example.superheroapi.model.IncidentType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeclaredIncidentDTO {
    @JsonProperty("id")
    private int id;

    @JsonProperty("latitude")
    private double latitude;

    @JsonProperty("longitude")
    private double longitude;

    @JsonProperty("description")
    private String description;

    @JsonProperty("city")
    private String city;

    @JsonProperty("incidentType_id")
    private Integer incidentTypeId;

    @JsonProperty("superhero_id")
    private Integer superheroId;

    public DeclaredIncidentDTO(double latitude, double longitude, String description, String city, Integer incidentTypeId, Integer superheroId) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.city = city;
        this.incidentTypeId = incidentTypeId;
        this.superheroId = superheroId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getSuperheroId() {
        return superheroId;
    }

    public void setSuperheroId(Integer superheroId) {
        this.superheroId = superheroId;
    }

    public Integer getIncidentTypeId() {
        return incidentTypeId;
    }

    public void setIncidentTypeId(Integer incidentTypeId) {
        this.incidentTypeId = incidentTypeId;
    }
}
