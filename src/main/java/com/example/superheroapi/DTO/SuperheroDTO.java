package com.example.superheroapi.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class SuperheroDTO {
    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("latitude")
    private double latitude;

    @JsonProperty("longitude")
    private double longitude;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("imagePath")
    private String image;

    @JsonProperty("incidentTypes")
    private Set<Integer> incidentAffectations;

    public SuperheroDTO(String name, double latitude, double longitude, String phone, String image, Set<Integer> incidentAffectations) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.image = image;
        this.incidentAffectations = incidentAffectations;
    }

    public SuperheroDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Set<Integer> getIncidentAffectations() {
        return incidentAffectations;
    }

    public void setIncidentAffectations(Set<Integer> incidentAffectations) {
        this.incidentAffectations = incidentAffectations;
    }
}
