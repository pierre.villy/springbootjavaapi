package com.example.superheroapi.controller;

import com.example.superheroapi.exception.ResourceNotFoundException;
import com.example.superheroapi.model.IncidentType;
import com.example.superheroapi.model.Superhero;
import com.example.superheroapi.repository.IncidentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/incidentTypes")
public class IncidentTypeController {
    @Autowired
    private IncidentTypeRepository incidentTypeRepository;

    @GetMapping("/")
    public List<IncidentType> getAllIncidentTypes(){
        return this.incidentTypeRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<IncidentType> getIncidentTypeById(@PathVariable(value = "id") int incidentTypeId)
            throws ResourceNotFoundException {
        IncidentType incidentType = incidentTypeRepository.findById(incidentTypeId)
                .orElseThrow(() -> new ResourceNotFoundException("IncidentType not found for this id :: " + incidentTypeId));
        return ResponseEntity.ok().body(incidentType);
    }

    @PostMapping("/")
    public IncidentType createIncidentType(@Valid @RequestBody IncidentType incidentType) {
        return incidentTypeRepository.save(incidentType);
    }

    @PutMapping("/{id}")
    public ResponseEntity<IncidentType> updateIncidentType(@PathVariable(value = "id") int incidentTypeId,
                                                    @Valid @RequestBody IncidentType incidentTypeDetails) throws ResourceNotFoundException {
        IncidentType incidentType = incidentTypeRepository.findById(incidentTypeId)
                .orElseThrow(() -> new ResourceNotFoundException("IncidentType not found for this id :: " + incidentTypeId));

        incidentType.setName(incidentTypeDetails.getName());
        final IncidentType updatedIncidentType = incidentTypeRepository.save(incidentType);
        return ResponseEntity.ok(updatedIncidentType);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteIncidentType(@PathVariable(value = "id") int incidentTypeId)
            throws ResourceNotFoundException {
        IncidentType incidentType = incidentTypeRepository.findById(incidentTypeId)
                .orElseThrow(() -> new ResourceNotFoundException("IncidentType not found for this id :: " + incidentTypeId));

        incidentTypeRepository.delete(incidentType);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
