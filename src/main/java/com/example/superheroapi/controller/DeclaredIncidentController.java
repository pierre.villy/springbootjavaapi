package com.example.superheroapi.controller;

import com.example.superheroapi.DTO.DeclaredIncidentDTO;
import com.example.superheroapi.exception.ResourceNotFoundException;
import com.example.superheroapi.model.DeclaredIncident;
/*import com.example.superheroapi.repository.CityRepository;*/
import com.example.superheroapi.model.Superhero;
import com.example.superheroapi.repository.DeclaredIncidentRepository;
import com.example.superheroapi.repository.IncidentTypeRepository;
import com.example.superheroapi.repository.SuperheroRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/declaredIncidents")
public class DeclaredIncidentController {

    @Autowired
    private DeclaredIncidentRepository declaredIncidentRepository;

    @Autowired
    private SuperheroRepository superheroRepository;

    @Autowired
    private IncidentTypeRepository incidentTypeRepository;

    /*@Autowired
    private CityRepository cityRepository;*/

    @GetMapping("/")
    public List<DeclaredIncident> getAllDeclaredIncidents(){
        return this.declaredIncidentRepository.findAll();
    }

    @GetMapping("/getGeoJsonFormat")
    public String getGeoJsonFormat() throws JSONException, JsonProcessingException {
        List<DeclaredIncident> declaredIncidents = this.declaredIncidentRepository.findAll();

        JSONObject featureCollection = new JSONObject();
        featureCollection.put("type", "FeatureCollection");

        JSONArray features = new JSONArray();
        for (DeclaredIncident declaredIncident : declaredIncidents) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Feature");

            JSONObject properties = new JSONObject();
            properties.put("id", declaredIncident.getId());
            properties.put("description", declaredIncident.getDescription());
            properties.put("city", declaredIncident.getCity());
            properties.put("creation_date", declaredIncident.getCreationDate());
            properties.put("resolved", declaredIncident.isResolved());
            properties.put("superheroId", declaredIncident.getAffectedSuperhero().getId());
            properties.put("superheroName", declaredIncident.getAffectedSuperhero().getName());
            properties.put("incidentType", declaredIncident.getIncidentType().getName());


            jsonObject.put("properties", properties);

            JSONObject geometry = new JSONObject();
            geometry.put("type", "Point");

            JSONArray coordinates = new JSONArray();
            coordinates.put(declaredIncident.getLongitude());
            coordinates.put(declaredIncident.getLatitude());

            geometry.put("coordinates", coordinates);
            jsonObject.put("geometry",geometry);

            features.put(jsonObject);
        }

        featureCollection.put("features", features);
        return featureCollection.toString();
    }

    @GetMapping("/{id}")
    public ResponseEntity<DeclaredIncident> getDeclaredIncidentById(@PathVariable(value = "id") int declaredIncidentId)
            throws ResourceNotFoundException {
        DeclaredIncident declaredIncident = declaredIncidentRepository.findById(declaredIncidentId)
                .orElseThrow(() -> new ResourceNotFoundException("DeclaredIncident not found for this id :: " + declaredIncidentId));
        return ResponseEntity.ok().body(declaredIncident);
    }

    @PostMapping("/")
    @ResponseBody
    public ResponseEntity<DeclaredIncident> createDeclaredIncident(@Valid @RequestBody DeclaredIncidentDTO declaredIncidentDTO)
            throws ResourceNotFoundException{

        DeclaredIncident declaredIncident = new DeclaredIncident();
        declaredIncident.setLatitude(declaredIncidentDTO.getLatitude());
        declaredIncident.setLongitude(declaredIncidentDTO.getLongitude());
        declaredIncident.setDescription(declaredIncidentDTO.getDescription());
        declaredIncident.setCreationDate(new Date());
        /*declaredIncident.setCity(cityRepository.getById(declaredIncidentDTO.getCityId()));*/
        declaredIncident.setCity(declaredIncidentDTO.getCity());
        declaredIncident.setAffectedSuperhero(superheroRepository.getById(declaredIncidentDTO.getSuperheroId()));
        declaredIncident.setResolutionDate(null);
        declaredIncident.setResolved(false);
        declaredIncident.setIncidentType(incidentTypeRepository.getById(declaredIncidentDTO.getIncidentTypeId()));

        final DeclaredIncident createdIncident = declaredIncidentRepository.save(declaredIncident);
        return ResponseEntity.ok().body(createdIncident);
    }

    @PutMapping("/{id}")
    public ResponseEntity<DeclaredIncident> updateDeclaredIncident(@PathVariable(value = "id") int declaredIncidentId,
                                           @Valid @RequestBody DeclaredIncident declaredIncidentDetails) throws ResourceNotFoundException {
        DeclaredIncident declaredIncident = declaredIncidentRepository.findById(declaredIncidentId)
                .orElseThrow(() -> new ResourceNotFoundException("DeclaredIncident not found for this id :: " + declaredIncidentId));

        declaredIncident.setDescription(declaredIncidentDetails.getDescription());
        declaredIncident.setCity(declaredIncidentDetails.getCity());
        declaredIncident.setCreationDate(declaredIncidentDetails.getCreationDate());
        declaredIncident.setAffectedSuperhero(declaredIncidentDetails.getAffectedSuperhero());
        final DeclaredIncident updatedIncidentType = declaredIncidentRepository.save(declaredIncident);
        return ResponseEntity.ok(updatedIncidentType);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteDeclaredIncident(@PathVariable(value = "id") int declaredIncidentId)
            throws ResourceNotFoundException {
        DeclaredIncident declaredIncident = declaredIncidentRepository.findById(declaredIncidentId)
                .orElseThrow(() -> new ResourceNotFoundException("DeclaredIncident not found for this id :: " + declaredIncidentId));

        declaredIncidentRepository.delete(declaredIncident);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
