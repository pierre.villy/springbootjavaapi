package com.example.superheroapi.controller;

import java.awt.*;
import java.util.*;
import java.util.List;

import javax.validation.Valid;

import com.example.superheroapi.DTO.SuperheroDTO;
import com.example.superheroapi.model.IncidentType;
import com.example.superheroapi.repository.IncidentTypeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.superheroapi.model.Superhero;
import com.example.superheroapi.repository.SuperheroRepository;
import com.example.superheroapi.exception.ResourceNotFoundException;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/superheroes")
public class SuperheroController {

    @Autowired
    private SuperheroRepository superheroRepository;

    @Autowired
    private IncidentTypeRepository incidentTypeRepository;

    @GetMapping("/")
    public List<Superhero> getAllSuperheroes(){
        return this.superheroRepository.findAll();
    }

    @GetMapping("/getGeoJsonFormat")
    public String getGeoJsonFormat() throws JSONException, JsonProcessingException {
        List<Superhero> superheroes = this.superheroRepository.findAll();

        JSONObject featureCollection = new JSONObject();
        featureCollection.put("type", "FeatureCollection");

        JSONArray features = new JSONArray();
        for (Superhero superhero : superheroes) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Feature");

            JSONObject properties = new JSONObject();
            properties.put("id", superhero.getId());
            properties.put("name", superhero.getName());
            properties.put("image", superhero.getImagePath());
            properties.put("available", superhero.isAvailable());
            properties.put("phone", superhero.getPhone());

            JSONArray incidentsAffectations = new JSONArray();
            for(IncidentType incidentType : superhero.getIncidentAffectations()){
                incidentsAffectations.put(incidentType.getName());
            }
            properties.put("incidentAffectations", incidentsAffectations);

            jsonObject.put("properties", properties);

            JSONObject geometry = new JSONObject();
            geometry.put("type", "Point");

            JSONArray coordinates = new JSONArray();
            coordinates.put(superhero.getLongitude());
            coordinates.put(superhero.getLatitude());

            geometry.put("coordinates", coordinates);
            jsonObject.put("geometry",geometry);

            features.put(jsonObject);
        }

        featureCollection.put("features", features);
        return featureCollection.toString();
    }


    @GetMapping("/{id}")
    public ResponseEntity<Superhero> getSuperheroById(@PathVariable(value = "id") int superheroId)
        throws ResourceNotFoundException {
            Superhero superhero = superheroRepository.findById(superheroId)
                .orElseThrow(() -> new ResourceNotFoundException("Superhero not found for this id :: " + superheroId));
        return ResponseEntity.ok().body(superhero);
    }

    @PostMapping("/")
    @ResponseBody
    public ResponseEntity<Superhero> createSuperhero(@Valid @RequestBody SuperheroDTO superheroDTO)
        throws ResourceNotFoundException {
            Superhero superhero = new Superhero();
            superhero.setName(superheroDTO.getName());
            superhero.setLatitude(superheroDTO.getLatitude());
            superhero.setLongitude(superheroDTO.getLongitude());
            superhero.setPhone(superheroDTO.getPhone());
            superhero.setImagePath(superheroDTO.getImage());
            superhero.setAvailable(true);

            Set<IncidentType> incidentTypeSet = new HashSet<>();
            for(Integer incidentId : superheroDTO.getIncidentAffectations()){
                IncidentType incidentType = incidentTypeRepository.getById(incidentId);
                incidentTypeSet.add(incidentType);
            }
            superhero.setIncidentAffectations(incidentTypeSet);

            final Superhero createdSuperhero = superheroRepository.save(superhero);

            return ResponseEntity.ok().body(createdSuperhero);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Superhero> updateSuperhero(@PathVariable(value = "id") int superheroId,
                                                   @Valid @RequestBody Superhero superheroDetails) throws ResourceNotFoundException {
        Superhero superhero = superheroRepository.findById(superheroId)
                .orElseThrow(() -> new ResourceNotFoundException("Superhero not found for this id :: " + superheroId));

        superhero.setName((superheroDetails.getName()));
        superhero.setLatitude((superheroDetails.getLatitude()));
        superhero.setLongitude((superheroDetails.getLongitude()));
        superhero.setPhone((superheroDetails.getPhone()));
        superhero.setImagePath((superheroDetails.getImagePath()));
        superhero.setIncidentAffectations((superheroDetails.getIncidentAffectations()));
        superhero.setDeclaredIncidents(superheroDetails.getDeclaredIncidents());
        superhero.setAvailable(superheroDetails.isAvailable());
        final Superhero updatedSuperhero = superheroRepository.save(superhero);
        return ResponseEntity.ok(updatedSuperhero);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteSuperhero(@PathVariable(value = "id") int superheroId)
            throws ResourceNotFoundException {
        Superhero superhero = superheroRepository.findById(superheroId)
                .orElseThrow(() -> new ResourceNotFoundException("Superhero not found for this id :: " + superheroId));

        superheroRepository.delete(superhero);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
